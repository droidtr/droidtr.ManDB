package droidtr.mandb;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.R.*;
import android.widget.EditText;

public class Manpage extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manpage);
        WebView v = (WebView) findViewById(R.id.web);
        v.getSettings().setBuiltInZoomControls(true);
        v.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        v.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String request) {
                view.loadUrl(request);
                return true;
            }
        });
        v.loadUrl("file:///android_asset/index.html");


    }
    public void goman(View view){
        EditText et =(EditText) findViewById(R.id.et);
        WebView v = (WebView) findViewById(R.id.web);
        v.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String request) {
                view.loadUrl(request);
                return true;
            }
        });
        v.loadUrl("file:///android_asset/html/" + et.getText() + ".html");
    }


    public void back(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        WebView v = (WebView) findViewById(R.id.web);
        v.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String request) {
                view.loadUrl(request);
                return true;
            }
        });
        v.loadUrl("file:///android_asset/index.html");
    }
}
